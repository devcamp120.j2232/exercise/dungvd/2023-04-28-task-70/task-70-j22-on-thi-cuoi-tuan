package com.devcamp.chuongbai.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "chuong_sach")
public class Chuong {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @JoinColumn(name = "ma_chuong", unique = true)
    private String maChuong;

    @JoinColumn(name = "ten_chuong")
    private String tenChuong;

    @JoinColumn(name = "gioi_thieu")
    private String gioiThieu;

    @JoinColumn(name = "ten_nguoi_dich")
    private String tenNguoiDich;

    private long trang;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "chuong")
    private Set<Bai> bais;

    public Chuong() {
    }

    public Chuong(String maChuong, String tenChuong, String gioiThieu, String tenNguoiDich, long trang, Set<Bai> bais) {
        this.maChuong = maChuong;
        this.tenChuong = tenChuong;
        this.gioiThieu = gioiThieu;
        this.tenNguoiDich = tenNguoiDich;
        this.trang = trang;
        this.bais = bais;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMaChuong() {
        return maChuong;
    }

    public void setMaChuong(String maChuong) {
        this.maChuong = maChuong;
    }

    public String getTenChuong() {
        return tenChuong;
    }

    public void setTenChuong(String tenChuong) {
        this.tenChuong = tenChuong;
    }

    public String getGioiThieu() {
        return gioiThieu;
    }

    public void setGioiThieu(String gioiThieu) {
        this.gioiThieu = gioiThieu;
    }

    public String getTenNguoiDich() {
        return tenNguoiDich;
    }

    public void setTenNguoiDich(String tenNguoiDich) {
        this.tenNguoiDich = tenNguoiDich;
    }

    public long getTrang() {
        return trang;
    }

    public void setTrang(long trang) {
        this.trang = trang;
    }

    public Set<Bai> getBais() {
        return bais;
    }

    public void setBais(Set<Bai> bais) {
        this.bais = bais;
    }

    
}
