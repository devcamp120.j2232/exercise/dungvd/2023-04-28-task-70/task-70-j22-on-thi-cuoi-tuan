package com.devcamp.chuongbai.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "bai_viet")
public class Bai {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @JoinColumn(name = "ma_bai", unique = true)
    private String maBai;

    @JoinColumn(name = "ten_bai")
    private String tenBai;

    @JoinColumn(name = "gioi_thieu")
    private String gioiThieu;

    @JoinColumn(name = "trang")
    private long trang;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "chuong_id")
    @JsonIgnore
    private Chuong chuong;

    public Bai() {
    }

    public Bai(long id, String maBai, String tenBai, String gioiThieu, long trang, Chuong chuong) {
        this.id = id;
        this.maBai = maBai;
        this.tenBai = tenBai;
        this.gioiThieu = gioiThieu;
        this.trang = trang;
        this.chuong = chuong;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMaBai() {
        return maBai;
    }

    public void setMaBai(String maBai) {
        this.maBai = maBai;
    }

    public String getTenBai() {
        return tenBai;
    }

    public void setTenBai(String tenBai) {
        this.tenBai = tenBai;
    }

    public String getGioiThieu() {
        return gioiThieu;
    }

    public void setGioiThieu(String gioiThieu) {
        this.gioiThieu = gioiThieu;
    }

    public long getTrang() {
        return trang;
    }

    public void setTrang(long trang) {
        this.trang = trang;
    }

    public Chuong getChuong() {
        return chuong;
    }

    public void setChuong(Chuong chuong) {
        this.chuong = chuong;
    }

    
}
