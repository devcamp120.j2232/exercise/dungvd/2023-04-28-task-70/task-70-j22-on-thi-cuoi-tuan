package com.devcamp.chuongbai.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.chuongbai.model.Chuong;

public interface ChuongRepository extends JpaRepository <Chuong, Long>{
    
}
