package com.devcamp.chuongbai.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.chuongbai.model.Bai;

public interface BaiRepository extends JpaRepository<Bai, Long>{
    
}
