package com.devcamp.chuongbai;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChuongBaiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChuongBaiApplication.class, args);
	}

}
