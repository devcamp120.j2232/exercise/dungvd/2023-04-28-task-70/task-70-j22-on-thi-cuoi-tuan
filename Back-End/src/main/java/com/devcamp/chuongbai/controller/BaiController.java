package com.devcamp.chuongbai.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.chuongbai.model.Bai;
import com.devcamp.chuongbai.model.Chuong;
import com.devcamp.chuongbai.repository.BaiRepository;
import com.devcamp.chuongbai.repository.ChuongRepository;

@RestController
@CrossOrigin
public class BaiController {
    @Autowired
    ChuongRepository chuongRepository;

    @Autowired
    BaiRepository baiRepository;

    // API để lấy toàn bộ bài viết
    @GetMapping("/bais")
    public ResponseEntity<List<Bai>> getAll() {
        try {
            return new ResponseEntity<>(baiRepository.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API lấy bài viết thông qua id
    @GetMapping("/bai/{id}")
    public ResponseEntity<Bai> getBaiById(@PathVariable("id") long id) {
        try {
            return new ResponseEntity<>(baiRepository.findById(id).get(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API lấy ra toàn bộ bài của 1 album thông qua Id
    @GetMapping("/chuong/{chuongId}/bais")
    public ResponseEntity<Set<Bai>> getAllBaiOfChuong(@PathVariable("chuongId") long chuongId) {
        try {
            Chuong chuong = chuongRepository.findById(chuongId).get();
            return new ResponseEntity<>(chuong.getBais(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

     // Hàm tạo mới 1 bài
     @PostMapping("/chuong/{chuongId}/bais")
     public ResponseEntity<Bai> createBai(@PathVariable("chuongId") long chuongId, @RequestBody Bai pBai) {
         try {
             Chuong chuong = chuongRepository.findById(chuongId).get();
             pBai.setChuong(chuong);    
             return new ResponseEntity<>(baiRepository.save(pBai), HttpStatus.OK);
         } catch (Exception e) {
             return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
         }
     }

     // Hàm cập nhật 1 bài
     @PutMapping("/chuong/{chuongId}/bai/{baiId}")
     public ResponseEntity<Bai> updateBai(@PathVariable("baiId") long baiId, @PathVariable("chuongId") long chuongId, @RequestBody Bai pBai) {
         try {
             Bai bai = baiRepository.findById(baiId).get();
             Chuong chuong = chuongRepository.findById(chuongId).get();
             bai.setChuong(chuong);
             bai.setMaBai(pBai.getMaBai());
             bai.setTenBai(pBai.getTenBai());
             bai.setTrang(pBai.getTrang());  
             bai.setGioiThieu(pBai.getGioiThieu());
             return new ResponseEntity<>(baiRepository.save(bai), HttpStatus.OK);
 
         } catch (Exception e) {
             return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
         }
     }

     // Hàm xóa 1 bài
    @DeleteMapping("/bai/{id}")
    public ResponseEntity<Bai> deleteBai(@PathVariable("id") long id) {
        try {
            baiRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Hàm xóa tất cả bài
    @DeleteMapping("/bais")
    public ResponseEntity<Bai> deleteAllBais() {
        try {
            baiRepository.deleteAll();;
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}


