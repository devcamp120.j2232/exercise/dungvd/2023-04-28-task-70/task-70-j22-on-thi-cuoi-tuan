package com.devcamp.chuongbai.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.chuongbai.model.Chuong;
import com.devcamp.chuongbai.repository.ChuongRepository;

@RestController
@CrossOrigin
public class ChuongController {
    @Autowired
    ChuongRepository chuongRepository;

    // API để lấy toàn bộ chương
    @GetMapping("/chuongs")
    public ResponseEntity<List<Chuong>> getAllChuong() {
        try {
            return new ResponseEntity<>(chuongRepository.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API lấy thông tin chương thông qua id
    @GetMapping("/chuong/{id}")
    public ResponseEntity<Chuong> getChuongById(@PathVariable("id") long id) {
        try {
            return new ResponseEntity<>(chuongRepository.findById(id).get(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API tạo mới 1 chương
    @PostMapping("/chuongs")
    public ResponseEntity<Chuong> createChuong(@RequestBody Chuong pChuong) {
        try {
            return new ResponseEntity<>(chuongRepository.save(pChuong), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Hàm cập nhật 1 chuong
    @PutMapping("/chuong/{id}")
    public ResponseEntity<Chuong> updateChuong(@RequestBody Chuong pChuong, @PathVariable("id") long id) {
        try {
            Chuong chuong = chuongRepository.findById(id).get();
            chuong.setMaChuong(pChuong.getMaChuong());
            chuong.setTenChuong(pChuong.getTenChuong());
            chuong.setTrang(pChuong.getTrang());
            chuong.setTenNguoiDich(pChuong.getTenNguoiDich());
            chuong.setGioiThieu(pChuong.getGioiThieu());
            return new ResponseEntity<>(chuongRepository.save(chuong), HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Hàm xóa 1 album
    @DeleteMapping("/chuong/{id}")
    public ResponseEntity<Chuong> deleteChuong(@PathVariable("id") long id) {
        try {
            chuongRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Hàm xóa tất cả chương
    @DeleteMapping("/chuongs")
    public ResponseEntity<Chuong> deleteAllChuongs() {
        try {
            chuongRepository.deleteAll();;
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
