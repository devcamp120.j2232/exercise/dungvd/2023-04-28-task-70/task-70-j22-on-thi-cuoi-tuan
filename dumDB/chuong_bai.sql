-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 01, 2023 at 03:09 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.0.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chuong_bai`
--

-- --------------------------------------------------------

--
-- Table structure for table `bai_viet`
--

CREATE TABLE `bai_viet` (
  `id` bigint(20) NOT NULL,
  `gioi_thieu` varchar(255) DEFAULT NULL,
  `ma_bai` varchar(255) DEFAULT NULL,
  `ten_bai` varchar(255) DEFAULT NULL,
  `trang` bigint(20) NOT NULL,
  `chuong_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `bai_viet`
--

INSERT INTO `bai_viet` (`id`, `gioi_thieu`, `ma_bai`, `ten_bai`, `trang`, `chuong_id`) VALUES
(1, '324', '234', '324', 234, 6),
(4, '234', '342a', '234', 324, 6),
(5, '234', '342as', '234', 324, 6),
(6, '234', '342aa', '234', 324, 7),
(7, '234ad', '342aaád', '234sad', 5, 7);

-- --------------------------------------------------------

--
-- Table structure for table `chuong_sach`
--

CREATE TABLE `chuong_sach` (
  `id` bigint(20) NOT NULL,
  `gioi_thieu` varchar(255) DEFAULT NULL,
  `ma_chuong` varchar(255) DEFAULT NULL,
  `ten_chuong` varchar(255) DEFAULT NULL,
  `ten_nguoi_dich` varchar(255) DEFAULT NULL,
  `trang` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `chuong_sach`
--

INSERT INTO `chuong_sach` (`id`, `gioi_thieu`, `ma_chuong`, `ten_chuong`, `ten_nguoi_dich`, `trang`) VALUES
(6, 'hoa', 'HDM', 'Hoa Đầu Mùa', 'Nguyễn Phi Sang', 5),
(7, 'hádk', 'CLMD', 'Chiếc Lá Mùa Đông', 'áđá', 6);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bai_viet`
--
ALTER TABLE `bai_viet`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKq0nepcno7hmd08hf0onii1lkc` (`chuong_id`);

--
-- Indexes for table `chuong_sach`
--
ALTER TABLE `chuong_sach`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bai_viet`
--
ALTER TABLE `bai_viet`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `chuong_sach`
--
ALTER TABLE `chuong_sach`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bai_viet`
--
ALTER TABLE `bai_viet`
  ADD CONSTRAINT `FKq0nepcno7hmd08hf0onii1lkc` FOREIGN KEY (`chuong_id`) REFERENCES `chuong_sach` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
